import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserModule } from '@angular/platform-browser';
import { MainHomeComponent } from './main-home/main-home.component';

import { SharedModule } from '../shared/shared.module';
import { MainHomeMobileComponent } from './main-home-mobile/main-home-mobile.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NavegationModule } from '../navegation/navegation.module';



@NgModule({
  declarations: [HomeComponent, MainHomeComponent, MainHomeMobileComponent],
  imports: [
    CommonModule,
    NgbModule,
    TranslateModule,
    BrowserModule,
    SharedModule,
    FontAwesomeModule,
    NavegationModule
  ],
  providers: [ ],
  exports: [
    HomeComponent
  ],
})
export class HomeModule { }
