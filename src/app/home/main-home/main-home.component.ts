import { LanguageService } from './../../shared/service/language.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-home',
  templateUrl: './main-home.component.html',
  styleUrls: ['./main-home.component.css']
})
export class MainHomeComponent implements OnInit {

  public language = '';

  constructor(
    private readonly  router: Router,
    private readonly languageService: LanguageService) { }

  ngOnInit() {
    this.subscribeToLanguage();
  }

  navigateToServices() {
    this.router.navigate(['/services']);
  }

  navigateToAbout() {
    this.router.navigate(['/about']);
  }

  navigateToContact() {
    this.router.navigate(['/contact']);
  }

  private subscribeToLanguage() {
    this.languageService
    .language$
    .subscribe(
      lang => this.language = lang
    );
  }

  changeLanguage(newLanguage: string) {
    this.languageService.changeLanguage(newLanguage);
  }
}
