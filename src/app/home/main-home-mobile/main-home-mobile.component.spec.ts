import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainHomeMobileComponent } from './main-home-mobile.component';

describe('MainHomeMobileComponent', () => {
  let component: MainHomeMobileComponent;
  let fixture: ComponentFixture<MainHomeMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainHomeMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainHomeMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
