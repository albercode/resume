import { Component, OnInit } from '@angular/core';
import { faAddressCard, faCog, faEnvelopeSquare } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-main-home-mobile',
  templateUrl: './main-home-mobile.component.html',
  styleUrls: ['./main-home-mobile.component.css']
})
export class MainHomeMobileComponent implements OnInit {

  faAddressCard = faAddressCard;
  faCog = faCog;
  faEnvelopeSquare = faEnvelopeSquare;

  constructor() { }

  ngOnInit() {
  }

}
