import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-section-sub-title',
  templateUrl: './section-sub-title.component.html',
  styleUrls: ['./section-sub-title.component.css']
})
export class SectionSubTitleComponent implements OnInit {

  @Input() text = '';

  constructor() { }

  ngOnInit() {
  }

}
