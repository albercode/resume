import { Component, OnInit } from '@angular/core';
import { LanguageService } from '../service/language.service';

@Component({
  selector: 'app-language-dropdown',
  templateUrl: './language-dropdown.component.html',
  styleUrls: ['./language-dropdown.component.css']
})
export class LanguageDropdownComponent implements OnInit {
  language = '';

  constructor(
    private readonly  languageService: LanguageService
  ) {
  }

  ngOnInit() {
    this.subscribeToLanguage();
  }

  private subscribeToLanguage() {
    this.languageService
    .language$
    .subscribe(
      lang => this.language = lang
    );
  }

  changeLanguage(newLanguage: string) {
    this.languageService.changeLanguage(newLanguage);
  }

}
