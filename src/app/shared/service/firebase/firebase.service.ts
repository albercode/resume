import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  constructor(private readonly firestore: AngularFirestore) { }

  public createDocument(key: string, value: any) {
    return this.firestore.collection(key).add({
      value
    });
  }
}
