import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  public language$ = new BehaviorSubject('');

  constructor(
    private readonly translate: TranslateService
  ) {
    this.changeLanguage(this.translate.currentLang);
  }

  changeLanguage(newLanguage: string) {
    this.translate.use(newLanguage);
    this.setLanguage(newLanguage);
  }

  private setLanguage(unformat: string) {
    if (unformat === 'en') {
      this.language$.next('EN');
    } else {
      this.language$.next('ES');
    }
  }
}
