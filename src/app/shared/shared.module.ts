import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SectionTitleComponent } from './section-title/section-title.component';
import { TranslateModule } from '@ngx-translate/core';
import { SectionSubTitleComponent } from './section-sub-title/section-sub-title.component';
import { LoadingComponent } from './loading/loading.component';
import { FooterComponent } from './footer/footer.component';
import { LanguageDropdownComponent } from './language-dropdown/language-dropdown.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [SectionTitleComponent, SectionSubTitleComponent, LoadingComponent, FooterComponent, LanguageDropdownComponent],
  imports: [
    CommonModule,
    TranslateModule,
    NgbModule,
    FontAwesomeModule,
    RouterModule
  ],
  exports: [
    SectionTitleComponent,
    SectionSubTitleComponent,
    LanguageDropdownComponent,
    FooterComponent
  ],
})
export class SharedModule { }
