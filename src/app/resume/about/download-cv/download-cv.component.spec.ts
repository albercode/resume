import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadCVComponent } from './download-cv.component';

describe('DownloadCVComponent', () => {
  let component: DownloadCVComponent;
  let fixture: ComponentFixture<DownloadCVComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DownloadCVComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadCVComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
