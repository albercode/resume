import { Component, OnInit, OnDestroy } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { LanguageService } from '../../../shared/service/language.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-download-cv',
  templateUrl: './download-cv.component.html',
  styleUrls: ['./download-cv.component.css']
})
export class DownloadCVComponent implements OnInit, OnDestroy {

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  public downloadPDFLink = 'assets/document/CVEnglish.pdf';
  public downloadPDFName = 'CVEnglish.pdf';

  constructor(private readonly  languageService: LanguageService) { }

  ngOnInit() {
    this.onLanguageChange();
  }

  private onLanguageChange() {
    this.languageService.language$
    .pipe(takeUntil(this.destroyed$))
    .subscribe(value => {
      if (value === 'EN') {
        this.downloadPDFLink = 'assets/document/CVEnglish.pdf';
        this.downloadPDFName = 'CVEnglish.pdf';
      } else {
        this.downloadPDFLink = 'assets/document/CVSpanish.pdf';
        this.downloadPDFName = 'CVSpanish.pdf';
      }
    });
  }



  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

}
