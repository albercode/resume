import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResumeComponent } from './resume.component';
import { AboutComponent } from './about/about.component';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserModule } from '@angular/platform-browser';
import { SharedModule } from '../shared/shared.module';
import { TimelineListComponent } from './timeline-list/timeline-list.component';
import { TimelineRowComponent } from './timeline-row/timeline-row.component';
import { DownloadCVComponent } from './about/download-cv/download-cv.component';



@NgModule({
  declarations: [ResumeComponent, AboutComponent, TimelineListComponent, TimelineRowComponent, DownloadCVComponent],
  imports: [
    CommonModule,
    TranslateModule,
    BrowserModule,
    SharedModule
  ],
  providers: [ ],
  exports: [
    ResumeComponent
  ],
})
export class ResumeModule { }
