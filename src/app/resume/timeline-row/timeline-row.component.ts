import { Timeline} from './../model/timeline.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-timeline-row',
  templateUrl: './timeline-row.component.html',
  styleUrls: ['./timeline-row.component.css']
})
export class TimelineRowComponent implements OnInit {

  @Input() timeline: Timeline;

  constructor() { }

  ngOnInit() {
  }

}
