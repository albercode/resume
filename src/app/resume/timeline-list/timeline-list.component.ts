import { Timeline } from './../model/timeline.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-timeline-list',
  templateUrl: './timeline-list.component.html',
  styleUrls: ['./timeline-list.component.css']
})
export class TimelineListComponent implements OnInit {

  public educationList: Array<Timeline>;
  public experienceList: Array<Timeline>;


  constructor() { }

  ngOnInit() {
    this.setEducationList();
    this.setExperienceList();
  }

  private setEducationList() {
    this.educationList = new Array<Timeline>();
    this.educationList.push(
      new Timeline(
        'about.education.uclm.title',
        'about.education.uclm.secondTitle',
        'about.education.uclm.time',
        ['about.education.uclm.content.one'],
      ),
      new Timeline(
        'about.education.cujae.title',
        'about.education.cujae.secondTitle',
        'about.education.cujae.time',
        ['about.education.cujae.content.one'],
      )
    );
  }

  private setExperienceList() {
    this.experienceList = new Array<Timeline>();
    this.experienceList.push(
      new Timeline(
        'about.experience.docpath.title',
        'about.experience.docpath.secondTitle',
        'about.experience.docpath.time',
        ['about.experience.docpath.content.one', 'about.experience.docpath.content.two', 'about.experience.docpath.content.three'],
      )
    );
  }

}
