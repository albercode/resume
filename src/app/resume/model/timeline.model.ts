

export class Timeline {
    title: string;
    secondTitle: string;
    time: string;
    content: Array<string>;
    constructor(
        title: string,
        secondTitle: string,
        time: string,
        content: Array<string>,
    ) {
        this.title = title;
        this.secondTitle = secondTitle;
        this.time = time;
        this.content = content;
    }
}
