import { Component, OnInit } from '@angular/core';
import { ServicesCardModel } from '../shared/services-card/model/services-card.model';
import { ServicesSkillModel } from '../shared/services-skill/model/services-skill.model';

@Component({
  selector: 'app-services-mobile',
  templateUrl: './services-mobile.component.html',
  styleUrls: ['./services-mobile.component.css']
})
export class ServicesMobileComponent implements OnInit {

  public title = 'services.mobile.title';
  public servicesCardList: Array<ServicesCardModel>;
  public servicesSkillList: Array<ServicesSkillModel>;

  constructor() { }

  ngOnInit() {
    this.setServiceCardList();
    this.setServicesSKillList();
  }

  private setServiceCardList() {
    this.servicesCardList = Array<ServicesCardModel>();
    this.servicesCardList.push(
      new ServicesCardModel(
        'about.education.uclm.title',
        'about.education.uclm.secondTitle',
        'about.education.uclm.time'
      ),
      new ServicesCardModel(
        'about.education.cujae.title',
        'about.education.cujae.secondTitle',
        'about.education.cujae.time'
      ),
      new ServicesCardModel(
        'about.education.uclm.title',
        'about.education.uclm.secondTitle',
        'about.education.uclm.time'
      ),
      new ServicesCardModel(
        'about.education.cujae.title',
        'about.education.cujae.secondTitle',
        'about.education.cujae.time'
      ),
      new ServicesCardModel(
        'about.education.uclm.title',
        'about.education.uclm.secondTitle',
        'about.education.uclm.time'
      ),
      new ServicesCardModel(
        'about.education.cujae.title',
        'about.education.cujae.secondTitle',
        'about.education.cujae.time'
      )
    );
  }

  private setServicesSKillList() {
    this.servicesSkillList = Array<ServicesSkillModel>();
    this.servicesSkillList.push(
      new ServicesSkillModel(
        'about.education.uclm.title',
        'about.education.uclm.secondTitle'
      ),
      new ServicesSkillModel(
        'about.education.uclm.title',
        'about.education.uclm.secondTitle'
      ),
      new ServicesSkillModel(
        'about.education.uclm.title',
        'about.education.uclm.secondTitle'
      ),
      new ServicesSkillModel(
        'about.education.uclm.title',
        'about.education.uclm.secondTitle'
      )
    );
  }

}
