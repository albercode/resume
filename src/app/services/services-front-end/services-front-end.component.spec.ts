import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesFrontEndComponent } from './services-front-end.component';

describe('ServicesFrontEndComponent', () => {
  let component: ServicesFrontEndComponent;
  let fixture: ComponentFixture<ServicesFrontEndComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicesFrontEndComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesFrontEndComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
