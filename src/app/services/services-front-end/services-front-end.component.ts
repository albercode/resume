import { ServicesCardModel } from './../shared/services-card/model/services-card.model';
import { Component, OnInit } from '@angular/core';
import { ServicesSkillModel } from '../shared/services-skill/model/services-skill.model';

@Component({
  selector: 'app-services-front-end',
  templateUrl: './services-front-end.component.html',
  styleUrls: ['./services-front-end.component.css']
})
export class ServicesFrontEndComponent implements OnInit {

  public title = 'services.backEnd.title';
  public servicesCardList: Array<ServicesCardModel>;
  public servicesSkillList: Array<ServicesSkillModel>;

  constructor() { }

  ngOnInit() {
    this.setServiceCardList();
    this.setServicesSKillList();
  }

  private setServiceCardList() {
    this.servicesCardList = Array<ServicesCardModel>();
    this.servicesCardList.push(
      new ServicesCardModel(
        'services.frontEnd.card.design.title',
        'services.frontEnd.card.design.content',
        'assets/images/services/frontend/card/design.svg'
      ),
      new ServicesCardModel(
        'services.frontEnd.card.develoment.title',
        'services.frontEnd.card.develoment.content',
        'assets/images/services/frontend/card/develoment.svg'
      ),
      new ServicesCardModel(
        'services.frontEnd.card.maintenance.title',
        'services.frontEnd.card.maintenance.content',
        'assets/images/services/frontend/card/maintenance.svg'
      ),
      new ServicesCardModel(
        'services.frontEnd.card.mobile.title',
        'services.frontEnd.card.mobile.content',
        'assets/images/services/frontend/card/mobile.svg'
      ),
      new ServicesCardModel(
        'services.common.card.qa.title',
        'services.common.card.qa.content',
        'assets/images/services/common/card/qa.svg'
      ),
    );
  }

  private setServicesSKillList() {
    this.servicesSkillList = Array<ServicesSkillModel>();
    this.servicesSkillList.push(
      new ServicesSkillModel(
        'Figma',
        'assets/images/services/frontend/skill/figma.png'
      ),
      new ServicesSkillModel(
        'Balsamiq',
        'assets/images/services/frontend/skill/Balsamiq.png'
      ),
      new ServicesSkillModel(
        'Angular',
        'assets/images/services/frontend/skill/angular.png'
      ),
      new ServicesSkillModel(
        'Vue',
        'assets/images/services/frontend/skill/vue.png'
      ),
      new ServicesSkillModel(
        'Jquery',
        'assets/images/services/frontend/skill/jquery.png'
      ),
      new ServicesSkillModel(
        'Html',
        'assets/images/services/frontend/skill/html.png'
      ),
      new ServicesSkillModel(
        'Css',
        'assets/images/services/frontend/skill/css.jpg'
      ),
      new ServicesSkillModel(
        'JavaScript',
        'assets/images/services/frontend/skill/js.png'
      ),
      new ServicesSkillModel(
        'TypeScript',
        'assets/images/services/frontend/skill/ts.png'
      ),
      new ServicesSkillModel(
        'RxJs',
        'assets/images/services/frontend/skill/rxjs.png'
      ),
      new ServicesSkillModel(
        'Bootstrap',
        'assets/images/services/frontend/skill/bootstrap.png'
      ),
      new ServicesSkillModel(
        'Material design',
        'assets/images/services/frontend/skill/md.png'
      ),
      new ServicesSkillModel(
        'Bulma',
        'assets/images/services/frontend/skill/Bulma.png'
      ),
      new ServicesSkillModel(
        'Jasmine',
        'assets/images/services/frontend/skill/jasmine.png'
      ),
      new ServicesSkillModel(
        'Karma',
        'assets/images/services/frontend/skill/karma.png'
      ),
      new ServicesSkillModel(
        'Cypress',
        'assets/images/services/frontend/skill/cypress.svg'
      ),

      new ServicesSkillModel(
        'Sonarqube',
        'assets/images/services/frontend/skill/sonar.png'
      ),
      new ServicesSkillModel(
        'Firebase',
        'assets/images/services/frontend/skill/firebase.png'
      ),
      new ServicesSkillModel(
        'Github',
        'assets/images/services/frontend/skill/github.jpg'
      ),
      new ServicesSkillModel(
        'GitLab',
        'assets/images/services/frontend/skill/gitlab.png'
      ),
    );
  }
}
