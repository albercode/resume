import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesCardListComponent } from './services-card-list.component';

describe('ServicesCardListComponent', () => {
  let component: ServicesCardListComponent;
  let fixture: ComponentFixture<ServicesCardListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicesCardListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesCardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
