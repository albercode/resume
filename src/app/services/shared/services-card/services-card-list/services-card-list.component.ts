import { Component, OnInit, Input } from '@angular/core';
import { ServicesCardModel } from '../model/services-card.model';

@Component({
  selector: 'app-services-card-list',
  templateUrl: './services-card-list.component.html',
  styleUrls: ['./services-card-list.component.css']
})
export class ServicesCardListComponent implements OnInit {

  @Input() servicesCardList: Array<ServicesCardModel>;

  constructor() { }

  ngOnInit() {
  }

}
