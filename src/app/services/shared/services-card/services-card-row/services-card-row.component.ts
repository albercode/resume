import { ServicesCardModel } from './../model/services-card.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-services-card-row',
  templateUrl: './services-card-row.component.html',
  styleUrls: ['./services-card-row.component.css']
})
export class ServicesCardRowComponent implements OnInit {

  @Input() servicesCardModel: ServicesCardModel;

  constructor() { }

  ngOnInit() {
  }

}
