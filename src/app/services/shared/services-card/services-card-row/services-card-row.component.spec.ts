import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesCardRowComponent } from './services-card-row.component';

describe('ServicesCardRowComponent', () => {
  let component: ServicesCardRowComponent;
  let fixture: ComponentFixture<ServicesCardRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicesCardRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesCardRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
