import { ServicesSkillModel } from './../services-skill/model/services-skill.model';
import { Component, OnInit, Input } from '@angular/core';
import { ServicesCardModel } from '../services-card/model/services-card.model';

@Component({
  selector: 'app-services-section',
  templateUrl: './services-section.component.html',
  styleUrls: ['./services-section.component.css']
})
export class ServicesSectionComponent implements OnInit {

  @Input() title: string;
  @Input() servicesCardList: Array<ServicesCardModel>;
  @Input() servicesSkillList: Array<ServicesSkillModel>;


  constructor() { }

  ngOnInit() {
  }

}
