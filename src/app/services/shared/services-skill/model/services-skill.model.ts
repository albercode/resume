
export class ServicesSkillModel {
    title: string;
    icon: string;

    constructor(
        title: string,
        icon: string
    ) {
        this.title = title;
        this.icon = icon;
    }
}
