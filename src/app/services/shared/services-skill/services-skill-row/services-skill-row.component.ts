import { Component, OnInit, Input } from '@angular/core';
import { ServicesSkillModel } from '../model/services-skill.model';

@Component({
  selector: 'app-services-skill-row',
  templateUrl: './services-skill-row.component.html',
  styleUrls: ['./services-skill-row.component.css']
})
export class ServicesSkillRowComponent implements OnInit {

  @Input() servicesSkillModel: ServicesSkillModel;


  constructor() { }

  ngOnInit() {
  }

}
