import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesSkillRowComponent } from './services-skill-row.component';

describe('ServicesSkillRowComponent', () => {
  let component: ServicesSkillRowComponent;
  let fixture: ComponentFixture<ServicesSkillRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicesSkillRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesSkillRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
