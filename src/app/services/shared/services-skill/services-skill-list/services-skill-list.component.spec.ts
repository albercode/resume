import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesSkillListComponent } from './services-skill-list.component';

describe('ServicesSkillListComponent', () => {
  let component: ServicesSkillListComponent;
  let fixture: ComponentFixture<ServicesSkillListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicesSkillListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesSkillListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
