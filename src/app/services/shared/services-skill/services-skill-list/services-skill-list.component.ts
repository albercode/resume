import { Component, OnInit, Input } from '@angular/core';
import { ServicesSkillModel } from '../model/services-skill.model';

@Component({
  selector: 'app-services-skill-list',
  templateUrl: './services-skill-list.component.html',
  styleUrls: ['./services-skill-list.component.css']
})
export class ServicesSkillListComponent implements OnInit {

  @Input() servicesSkillList: Array<ServicesSkillModel>;


  constructor() { }

  ngOnInit() {
  }

}
