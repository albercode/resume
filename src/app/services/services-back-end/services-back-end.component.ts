import { Component, OnInit } from '@angular/core';
import { ServicesSkillModel } from '../shared/services-skill/model/services-skill.model';
import { ServicesCardModel } from '../shared/services-card/model/services-card.model';

@Component({
  selector: 'app-services-back-end',
  templateUrl: './services-back-end.component.html',
  styleUrls: ['./services-back-end.component.css']
})
export class ServicesBackEndComponent implements OnInit {

  public title = 'services.backEnd.title';
  public servicesCardList: Array<ServicesCardModel>;
  public servicesSkillList: Array<ServicesSkillModel>;



  constructor() { }

  ngOnInit() {
    this.setServiceCardList();
    this.setServicesSKillList();
  }

  private setServiceCardList() {
    this.servicesCardList = Array<ServicesCardModel>();
    this.servicesCardList.push(
      new ServicesCardModel(
        'services.backEnd.card.rest.title',
        'services.backEnd.card.rest.content',
        'assets/images/services/backend/card/api.svg'
      ),
      new ServicesCardModel(
        'services.backEnd.card.integration.title',
        'services.backEnd.card.integration.content',
        'assets/images/services/backend/card/integration.svg'
      ),
      new ServicesCardModel(
        'services.backEnd.card.database.title',
        'services.backEnd.card.database.content',
        'assets/images/services/backend/card/database.svg'
      ),
      new ServicesCardModel(
        'services.backEnd.card.payment.title',
        'services.backEnd.card.payment.content',
        'assets/images/services/backend/card/payment.svg'
      ),
      new ServicesCardModel(
        'services.common.card.qa.title',
        'services.common.card.qa.content',
        'assets/images/services/common/card/qa.svg'
      )
    );
  }

  private setServicesSKillList() {
    this.servicesSkillList = Array<ServicesSkillModel>();
    this.servicesSkillList.push(
      new ServicesSkillModel(
        'Spring',
        'assets/images/services/backend/skill/spring.png'
      ),
      new ServicesSkillModel(
        'REST',
        'assets/images/services/backend/skill/rest.jpg'
      ),
      new ServicesSkillModel(
        'Nodejs',
        'assets/images/services/backend/skill/nodejs.png'
      ),
      new ServicesSkillModel(
        'Java',
        'assets/images/services/backend/skill/java.png'
      ),
      new ServicesSkillModel(
        'Kotlin',
        'assets/images/services/backend/skill/kotlin.svg'
      ),
      new ServicesSkillModel(
        'OAuth2',
        'assets/images/services/backend/skill/oauth2.png'
      ),
      new ServicesSkillModel(
        'MongoDB',
        'assets/images/services/backend/skill/mongodb.png'
      ),
      new ServicesSkillModel(
        'MySql',
        'assets/images/services/backend/skill/mysql.png'
      ),
      new ServicesSkillModel(
        'Oracle',
        'assets/images/services/backend/skill/oracle.png'
      ),
      new ServicesSkillModel(
        'Firebase',
        'assets/images/services/frontend/skill/firebase.png'
      ),
      new ServicesSkillModel(
        'Google Cloud',
        'assets/images/services/backend/skill/google.png'
      ),
      new ServicesSkillModel(
        'Amazon AWS',
        'assets/images/services/backend/skill/aws.jpg'
      ),
      new ServicesSkillModel(
        'Mockito',
        'assets/images/services/backend/skill/mockito.png'
      ),
      new ServicesSkillModel(
        'Docker',
        'assets/images/services/backend/skill/docker.png'
      ),
      new ServicesSkillModel(
        'Sonarqube',
        'assets/images/services/frontend/skill/sonar.png'
      ),
      new ServicesSkillModel(
        'Github',
        'assets/images/services/frontend/skill/github.jpg'
      ),
      new ServicesSkillModel(
        'GitLab',
        'assets/images/services/frontend/skill/gitlab.png'
      ),
    );
  }
}
