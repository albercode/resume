import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesBackEndComponent } from './services-back-end.component';

describe('ServicesBackEndComponent', () => {
  let component: ServicesBackEndComponent;
  let fixture: ComponentFixture<ServicesBackEndComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicesBackEndComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesBackEndComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
