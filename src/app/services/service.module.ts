import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServicesComponent } from './services.component';
import { ServicesCardListComponent } from './shared/services-card/services-card-list/services-card-list.component';
import { ServicesCardRowComponent } from './shared/services-card/services-card-row/services-card-row.component';
import { ServicesSkillListComponent } from './shared/services-skill/services-skill-list/services-skill-list.component';
import { ServicesSkillRowComponent } from './shared/services-skill/services-skill-row/services-skill-row.component';
import { ServicesFrontEndComponent } from './services-front-end/services-front-end.component';
import { ServicesBackEndComponent } from './services-back-end/services-back-end.component';
import { ServicesMobileComponent } from './services-mobile/services-mobile.component';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserModule } from '@angular/platform-browser';
import { SharedModule } from '../shared/shared.module';
import { ServicesSectionComponent } from './shared/services-section/services-section.component';



@NgModule({
  declarations: [
    ServicesComponent,
    ServicesCardListComponent,
    ServicesCardRowComponent,
    ServicesSkillListComponent,
    ServicesSkillRowComponent,
    ServicesFrontEndComponent,
    ServicesBackEndComponent,
    ServicesMobileComponent,
    ServicesSectionComponent],
  imports: [
    CommonModule,
    TranslateModule,
    BrowserModule,
    SharedModule
  ],
  exports: [
    ServicesComponent
  ]
})
export class ServicesModule { }
