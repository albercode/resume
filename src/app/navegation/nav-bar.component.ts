import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { LanguageService } from '../shared/service/language.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  isCollapsed = true;
  faBars = faBars;
  language = '';

  constructor(
    private readonly  languageService: LanguageService
  ) {
  }

  ngOnInit() {
    this.subscribeToLanguage();
  }

  toggleNavbar() {
    this.isCollapsed = !this.isCollapsed;
  }

  private subscribeToLanguage() {
    this.languageService
    .language$
    .subscribe(
      lang => this.language = lang
    );
  }

  changeLanguage(newLanguage: string) {
    this.languageService.changeLanguage(newLanguage);
  }



}
