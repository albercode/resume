import { Component, OnInit } from '@angular/core';
import { LanguageService } from 'src/app/shared/service/language.service';
import { faBars } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-nav-bar-desktop',
  templateUrl: './nav-bar-desktop.component.html',
  styleUrls: ['./nav-bar-desktop.component.css']
})
export class NavBarDesktopComponent implements OnInit {

  isCollapsed = true;
  faBars = faBars;
  language = '';

  constructor(
    private readonly  languageService: LanguageService
  ) {
  }

  ngOnInit() {
    this.subscribeToLanguage();
  }

  toggleNavbar() {
    this.isCollapsed = !this.isCollapsed;
    this.scrollTop();
  }

  scrollTop() {
    window.scrollTo(0, 0);
  }

  private subscribeToLanguage() {
    this.languageService
    .language$
    .subscribe(
      lang => this.language = lang
    );
  }

  changeLanguage(newLanguage: string) {
    this.languageService.changeLanguage(newLanguage);
  }

}
