import { Component, OnInit, Input } from '@angular/core';
import { LanguageService } from 'src/app/shared/service/language.service';
import { faAddressCard, faCog, faEnvelopeSquare, faHome } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-nav-bar-mobile',
  templateUrl: './nav-bar-mobile.component.html',
  styleUrls: ['./nav-bar-mobile.component.css']
})
export class NavBarMobileComponent implements OnInit {

  faAddressCard = faAddressCard;
  faCog = faCog;
  faEnvelopeSquare = faEnvelopeSquare;
  faHome = faHome;

  constructor() { }

  ngOnInit() {
  }


}
