import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavBarIconButtonComponent } from './nav-bar-icon-button.component';

describe('NavBarIconButtonComponent', () => {
  let component: NavBarIconButtonComponent;
  let fixture: ComponentFixture<NavBarIconButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavBarIconButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavBarIconButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
