import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-nav-bar-icon-button',
  templateUrl: './nav-bar-icon-button.component.html',
  styleUrls: ['./nav-bar-icon-button.component.css']
})
export class NavBarIconButtonComponent implements OnInit {

  @Input() icon: any;
  @Input() title: string;
  @Input() url: string;

  constructor() { }

  ngOnInit() {
  }

  scrollTop() {
    window.scrollTo(0, 0);
  }
}
