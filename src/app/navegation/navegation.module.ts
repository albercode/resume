import { SharedModule } from './../shared/shared.module';
import { RouterModule } from '@angular/router';
import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';

import { NavBarComponent } from './nav-bar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NavBarDesktopComponent } from './nav-bar-desktop/nav-bar-desktop.component';
import { NavBarIconButtonComponent } from './nav-bar-mobile/nav-bar-icon-button/nav-bar-icon-button.component';
import { NavBarMobileComponent } from './nav-bar-mobile/nav-bar-mobile.component';



@NgModule({
  declarations: [ NavBarComponent, NavBarMobileComponent, NavBarDesktopComponent, NavBarIconButtonComponent],
  imports: [
    CommonModule,
    NgbModule,
    FontAwesomeModule,
    RouterModule,
    TranslateModule
  ],
  exports: [
    NavBarComponent,
    NavBarIconButtonComponent
  ],

})
export class NavegationModule { }
