import { ContactModule } from './../contact.module';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class ContactService {

  public formSubmitted$ = new BehaviorSubject<boolean>(false);

  constructor() { }
}
