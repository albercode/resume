import { Router } from '@angular/router';
import { faLinkedin, faStackOverflow } from '@fortawesome/free-brands-svg-icons';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.css']
})
export class PersonalInfoComponent implements OnInit {

  faLinkedin = faLinkedin;
  faStackOverflow = faStackOverflow;

  constructor(private readonly router: Router) { }

  ngOnInit() {
  }

  public routeToExternallink(link: string) {
    this.router.navigate([]).then(result => { window.open(link, '_blank'); });
  }
}
