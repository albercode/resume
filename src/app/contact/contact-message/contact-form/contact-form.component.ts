import { FirebaseService } from './../../../shared/service/firebase/firebase.service';
import { ContactService } from '../../services/contact.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css']
})
export class ContactFormComponent implements OnInit {

  contactForm: FormGroup;
  submitted = false;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly contactService: ContactService,
    private readonly firebaseService: FirebaseService) { }

  ngOnInit() {
    this.generateContactForm();
  }

  private generateContactForm() {
    this.contactForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      message: ['', Validators.required],
    });
  }

  get name() {
    return this.contactForm.get('name');
  }

  get email() {
    return this.contactForm.get('email');
  }

  get message() {
    return this.contactForm.get('message');
  }

  onSubmit() {
    this.submitted = true;
    if (this.contactForm.invalid) {
      return;
    }
    this.firebaseService.createDocument('message', this.contactForm.value);
    this.resetForm();
    this.contactService.formSubmitted$.next(true);
  }

  resetForm() {
    this.submitted = false;
    this.contactForm.reset();
  }


}
