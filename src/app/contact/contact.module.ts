import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SharedModule } from './../shared/shared.module';
import { ContactFormComponent } from './contact-message/contact-form/contact-form.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from './contact.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserModule } from '@angular/platform-browser';
import { ContactService } from './services/contact.service';
import { ContactMessageComponent } from './contact-message/contact-message.component';
import { ContactMessageSendComponent } from './contact-message-send/contact-message-send.component';
import { PersonalInfoComponent } from './personal-info/personal-info.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [ContactComponent, ContactFormComponent, ContactMessageSendComponent, ContactMessageComponent, PersonalInfoComponent],
  imports: [
    CommonModule,
    BrowserModule,
    ReactiveFormsModule,
    TranslateModule,
    SharedModule,
    RouterModule,
    FontAwesomeModule
  ],
  providers: [ContactService ],
  exports: [
    ContactComponent
  ],
})
export class ContactModule { }
