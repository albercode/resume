import { Component, OnInit, OnDestroy } from '@angular/core';
import { ContactService } from './services/contact.service';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit, OnDestroy {
  formSubmitted = false;
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  constructor(private contactService: ContactService) { }

  ngOnInit() {
    this.setFormSubmitted();
  }

  private setFormSubmitted() {
    this.contactService.formSubmitted$
    .pipe(takeUntil(this.destroyed$))
    .subscribe(value => this.formSubmitted = value);
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }
}
