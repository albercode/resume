import { ContactService } from '../services/contact.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact-message-send',
  templateUrl: './contact-message-send.component.html',
  styleUrls: ['./contact-message-send.component.css']
})
export class ContactMessageSendComponent implements OnInit {

  constructor(private contactService: ContactService) { }

  ngOnInit() {
  }

  public writeAgain() {
    this.contactService.formSubmitted$.next(false);
  }

}
