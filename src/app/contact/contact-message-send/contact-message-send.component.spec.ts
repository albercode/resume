import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactMessageSendComponent } from './contact-message-send.component';

describe('ContactMessageSendComponent', () => {
  let component: ContactMessageSendComponent;
  let fixture: ComponentFixture<ContactMessageSendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactMessageSendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactMessageSendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
