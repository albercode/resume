import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NavigationStart, NavigationEnd, NavigationError, NavigationCancel, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import {  BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'AlberCaro';
  loading$ = new BehaviorSubject(true);

  constructor(
    private readonly translate: TranslateService,
    private router: Router
  ) {
    this.translate.addLangs(['en', 'es']);
    this.translate.setDefaultLang('en');

    const browserLang = translate.getBrowserLang();
    this.translate.use(browserLang.match(/en|es/) ? browserLang : 'en');
  }

  ngOnInit() {
    this.setLoadingOnRoute();
  }

  private setLoadingOnRoute() {
    this.router.events
      .pipe(
        filter(
          event =>
            event instanceof NavigationStart ||
            event instanceof NavigationEnd ||
            event instanceof NavigationCancel ||
            event instanceof NavigationError,
        ),
      )
      .subscribe(event => {
        if (event instanceof NavigationStart) {
          this.loading$.next(true);
          return;
        }
        this.loading$.next(false);
      });
  }
}
