// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBCb9unUuZEIWquuD1b3io6aHUJANjR7bc',
    authDomain: 'my-resume-angular.firebaseapp.com',
    databaseURL: 'https://my-resume-angular.firebaseio.com',
    projectId: 'my-resume-angular',
    storageBucket: 'my-resume-angular.appspot.com',
    messagingSenderId: '324541347056',
    appId: '1:324541347056:web:3e7c925c2c14a49b4ebe35',
    measurementId: 'G-HS9NFV585T'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
