module.exports = function (grunt) {

    grunt.loadNpmTasks('grunt-war');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        war: {
            target: {
                options: {
                    war_dist_folder: 'dist',
                    war_name: 'resume',
                    webxml_display_name: 'resume',
                    webxml_webapp_version: '1.0.0',
                    webxml_webapp_extras: [
                        "<filter>\n<filter-name>UrlRewriteFilter</filter-name>\n<filter-class>org.tuckey.web.filters.urlrewrite.UrlRewriteFilter</filter-class>\n</filter>\n<filter-mapping>\n<filter-name>UrlRewriteFilter</filter-name>\n<url-pattern>/*</url-pattern>\n<dispatcher>REQUEST</dispatcher>\n<dispatcher>FORWARD</dispatcher>\n</filter-mapping>"
                    ],
                },
                files: [
                    {
                        expand: true,
                        cwd: 'dist',
                        src: ['**'],
                        dest: ''
                    },
                    {
                        expand: true,
                        cwd: 'src/assets/deployment',
                        src: ['**'],
                        dest: 'WEB-INF'
                    }
                ]
            }
        }
    });





    grunt.registerTask('default', ['war']);



};
